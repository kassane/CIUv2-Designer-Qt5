#-------------------------------------------------
#
# Project created by QtCreator 2017-12-10T11:47:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Designer
TEMPLATE = app
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RC_ICONS += $$PWD/Icons/Icon.ico

isEmpty(TARGET_EXT) {
    win32 {
        TARGET_CUSTOM_EXT = .exe
        }
} else {
    TARGET_CUSTOM_EXT = $${TARGET_EXT}
}

win32 {
    DEPLOY_COMMAND = windeployqt
}

CONFIG( debug, debug|release ) {
    # debug
    DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/debug/$${TARGET}$${TARGET_CUSTOM_EXT}))
} else {
    # release
    DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/release/$${TARGET}$${TARGET_CUSTOM_EXT}))
}

# Use += ao invés do = se for usar multiplos QMAKE_POST_LINKs
QMAKE_POST_LINK = $${DEPLOY_COMMAND} $${DEPLOY_TARGET}

SOURCES += \
        $$PWD/src/main.cpp \
        $$PWD/src/mydesigner.cpp \
        $$PWD/src/ciuoptions.cpp \
        $$PWD/src/inifiles.cpp \
        $$PWD/src/equalizer.cpp \
        $$PWD/src/button.cpp \
        $$PWD/src/draganddrop.cpp


HEADERS += \
            $$PWD/src/mydesigner.hpp \
            $$PWD/src/ciuoptions.hpp \
            $$PWD/src/inifiles.hpp \
            $$PWD/src/equalizer.hpp \
            $$PWD/src/button.hpp \
            $$PWD/src/draganddrop.hpp


FORMS += $$PWD/ui/mydesigner.ui

RESOURCES += icons.qrc

#TRANSLATIONS += pt_BR.UFT-8.ts
