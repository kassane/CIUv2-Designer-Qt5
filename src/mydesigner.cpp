#include "mydesigner.hpp"
#include "ciuoptions.hpp"
#include "inifiles.hpp"
#include <QColor>
#include <QColorDialog>
#include <QFileDialog>
#include <QMessageBox>
#include "ui_mydesigner.h"

static IniFiles ini;

MyDesigner::MyDesigner(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MyDesigner) {
    ui->setupUi(this);
    m_si = new SmallInstaller(this);
    m_ar = new Autorun(this);
    m_in = new Installer(this);

    m_ar->setMinimumSize(QSize(1180, 660));
    m_in->setMinimumSize(QSize(1000, 660));

    ui->statusBar->showMessage(tr("Welcome to CIUv2 Designer"), 50000);
    ini.setgroup("InstallOptions");
    // ini.setCategory("ApplicationName");
    ini.Load();

    setImages();
    CustomLayout();

    connections();
    // m_ar->setPos(ui->spinBox->value(),ui->spinBox_2->value());
}

MyDesigner::~MyDesigner() {
    m_ar->deleteLater();
    m_si->deleteLater();
    m_in->deleteLater();
    delete ui;
}

void MyDesigner::on_actionAbout_triggered() {
    qApp->beep();
    qApp->aboutQt();
}

void MyDesigner::on_actionExit_triggered() {
    qApp->beep();
    close();
}

void MyDesigner::on_tBtnAR_normal_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("StandardButtonAR", cor.name().replace("#","$"),
                 "FontColor");  // m_ar->ColorsBtn(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtnAR_select_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("StandardButtonAR", cor.name().replace("#","$"),
                 "FontColorSelected");  // m_ar->ColorsBtnSelect(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtnAR_click_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("StandardButtonAR", cor.name().replace("#","$"),
                 "FontColorClicked");  // m_ar->ColorsBtnClicked(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtnAR_disable_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    ini.Edit("StandardButtonAR", cor.name().replace("#","$"),
             "FontColorDisabled");  // m_ar->ColorsBtnDisabled(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtn_Normal_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("StandardButton", cor.name().replace("#","$"),
                 "FontColor");  // m_in->ColorsBtn(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtn_select_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("StandardButton", cor.name().replace("#","$"),
                 "FontColorSelected");  // m_in->ColorsBtnSelect(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtn_click_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("StandardButton", cor.name().replace("#","$"),
                 "FontColorClicked");  // m_in->ColorsBtnClicked(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtn_disable_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("StandardButton", cor.name().replace("#","$"),
                 "FontColorDisabled");  // m_in->ColorsBtnDisabled(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtnSM_normal_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("SmallButton", cor.name().replace("#","$"),
                 "FontColor");  // m_si->ColorsBtn(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtnSM_select_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("SmallButton", cor.name().replace("#","$"),
                 "FontColorSelected");  // m_si->ColorsBtnSelect(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtnSM_click_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    ini.Edit("SmallButton", cor.name().replace("#","$"),
             "FontColorCliked");  // m_si->ColorsBtnClicked(cor.name().replace("#","$"));
}

void MyDesigner::on_tBtnSM_disable_clicked() {
    QColor cor = QColorDialog::getColor(Qt::white, this, tr("Choose Color"));
    if (cor.isValid())
        ini.Edit("StandardButton", cor.name().replace("#","$"),
                 "FontColorDisabled");  // m_si->ColorsBtnDisabled(cor.name().replace("#","$"));
}

void MyDesigner::CustomLayout() {
    // Layout Background
    QGridLayout *GLb = new QGridLayout(this);
    // Layout Autorun
    QGridLayout *GLa = new QGridLayout(this);
    // Layout Installer
    QGridLayout *GLi = new QGridLayout(this);
    // Layout SmallInstaller
    QGridLayout *GLsi = new QGridLayout(this);
    // Layout ButtonAR
    QGridLayout *GAR = new QGridLayout(this);
    // Layout Button (installer)
    QGridLayout *GIn = new QGridLayout(this);
    // Layout SmallButtons
    // QGridLayout *GLsb = new QGridLayout(this);

    GLa->addWidget(ui->img_autorun);
    GLi->addWidget(ui->img_installer);
    GLb->addWidget(ui->img_background);
    GLsi->addWidget(ui->img_smallinstaller);
    GLsi->addWidget(m_si, 0, 0, 0, 0);
    GAR->addWidget(m_ar, 0, 0, 0, 0);
    GIn->addWidget(m_in, 0, 0, 0, 0);
    // GLsi->addLayout(GLsb,0,0,0,0, Qt::AlignBottom);
    // GLsb->setMargin(30);
    GLb->addLayout(GLsi, 0, 0, 0, 0, Qt::AlignBottom | Qt::AlignHCenter);

    GLa->addLayout(GAR, 0, 0, 0, 0, Qt::AlignCenter);
    GLi->addLayout(GIn, 0, 0, 0, 0, Qt::AlignCenter);

    ui->scrollArea->setLayout(GLa);

    ui->scrollArea_2->setLayout(GLi);

    ui->scrollArea_3->setLayout(GLb);
    //    GLb->deleteLater();
    //    GLa->deleteLater();
    //    GLi->deleteLater();
    //    GLsi->deleteLater();
    //    GLsb->deleteLater();
}

void MyDesigner::setImages() {
    ui->img_autorun->setPixmap(
        m_ar->CustomInstallerImage());  //.scaled(ui->scrollArea->width(),ui->scrollArea->height()));
    ui->img_installer->setPixmap(
        m_in->CustomInstallerImage());  //.scaled(ui->scrollArea->width(),ui->scrollArea->height(),Qt::KeepAspectRatio));
    ui->img_background->setPixmap(
        m_si->BackgroundImage());  //.scaled(ui->img_autorun->width(),
                                   // ui->img_autorun->height(),Qt::KeepAspectRatioByExpanding));
    ui->img_smallinstaller->setPixmap(
        m_si->CustomInstallerImage());  //.scaled(50,100,Qt::KeepAspectRatioByExpanding));

    // m_ar->Uninstall()->setAttribute(Qt::WA_DeleteOnClose);
    //    m_si->BtnMusic()->setAttribute(Qt::WA_DeleteOnClose);
    //    m_si->BtnCancel()->setAttribute(Qt::WA_DeleteOnClose);
    //    // m_ar->Uninstall()->setScaledContents(true);
    //    m_si->BtnMusic()->setScaledContents(true);
    //    m_si->BtnCancel()->setScaledContents(true);
    // ui->sb_sm_top->setValue(m_si->geometry().y());

    ui->img_background->setScaledContents(true);
    ui->img_autorun->setScaledContents(true);
    ui->img_installer->setScaledContents(true);
    ui->img_smallinstaller->setScaledContents(true);

    ui->img_smallinstaller->setFixedSize(QSize(500, 300));
}

void MyDesigner::connections() {
    // Connect SpinBox to Drag Position
    connect(m_ar, &Autorun::PosX, ui->top_spinBox, &QSpinBox::setValue);
    connect(m_ar, &Autorun::PosY, ui->left_spinBox, &QSpinBox::setValue);
    connect(m_in, &Installer::PosX, ui->top_inSP, &QSpinBox::setValue);
    connect(m_in, &Installer::PosY, ui->left_inSP, &QSpinBox::setValue);
    connect(m_si, &SmallInstaller::PosX, ui->top_smSP, &QSpinBox::setValue);
    connect(m_si, &SmallInstaller::PosY, ui->left_smSP, &QSpinBox::setValue);
}

void MyDesigner::on_action_Save_Project_triggered() {
    qApp->beep();
    ini.Save();
}

void MyDesigner::on_actionInfo_triggered() {
    qApp->beep();
    QMessageBox::about(this, tr("About"),
                       tr("Copyright: Kassane© 2017\nDeveloper: "
                          "Kassane\nCriative Idea: Yener90"
                          "\n\nSpecial Thanks: yener90, Razor12911, BAMsE"));
}

void MyDesigner::on_actionNew_Project_triggered() {
    auto filename = QFileDialog::getSaveFileName(
        this, tr("Save File"), QDir::currentPath(), tr("Ini Files (*.ini)"));
    ini.setfilename(filename);
}

void MyDesigner::on_action_Open_Project_triggered() {
    auto _filename = QFileDialog::getOpenFileName(
        this, tr("Open File"), QDir::currentPath(), tr("Ini Files (*.ini)"));
    ini.setfilename(_filename);
}
